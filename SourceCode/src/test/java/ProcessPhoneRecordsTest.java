import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ProcessPhoneRecordsTest {

    private static List<PhoneRecord> dummyRecords;
    private static ExtHashMap<String, List<String>> expectedHashMap;

    @Before
    public void setUp() {
        dummyRecords = new LinkedList<>();
        expectedHashMap = new ExtHashMap<>();
        dummyRecords.add(new PhoneRecord("101", "123-4566"));
        dummyRecords.add(new PhoneRecord("101", "321-4566"));
        dummyRecords.add(new PhoneRecord("001", "789-4566"));
        dummyRecords.add(new PhoneRecord("002", "345-4566"));
        MergeCSV.clearWorkQueue();
        List<String> phoneNums1 = new LinkedList<>(Arrays.asList("123-4566", "321-4566"));
        List<String> phoneNums2 = new LinkedList<>(Arrays.asList("789-4566"));
        List<String> phoneNums3 = new LinkedList<>(Arrays.asList("345-4566"));
        expectedHashMap.put("101", phoneNums1);
        expectedHashMap.put("001", phoneNums2);
        expectedHashMap.put("002", phoneNums3);
    }

    @After
    public void cleanUp() {
        MergeCSV.clearWorkQueue();
    }

    /**
     * This test is to verify the processPhoneRecord thread would produce correct HashMap
     */
    @Test
    public void testMergeHashMap() throws InterruptedException {
        Runnable thread = new ProcessPhoneRecords(dummyRecords);
        ExecutorService executor = Executors.newFixedThreadPool(1);
        executor.execute(thread);
        executor.shutdown();

        // wait current thread to be finished tasks for 2 sec
        if(!executor.awaitTermination(2, TimeUnit.SECONDS)){
            // force them to quit by interrupting
            executor.shutdownNow();
        }
        ExtHashMap<String, List<String>> mergedMap = MergeCSV.pollWorkQueue();
        assertNotNull(mergedMap);

        for(Map.Entry<String, List<String>> cur: mergedMap.entrySet()){
            List<String> expectedList = expectedHashMap.get(cur.getKey());
            List<String> returnedList = cur.getValue();
            Collections.sort(expectedList);
            Collections.sort(returnedList);
            assertEquals(true, expectedList.equals(returnedList));
        }
    }
}
