
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.sound.sampled.Line;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class MergeHashMapTest {

    private ExtHashMap<String, List<String>> dumyMap1;
    private ExtHashMap<String, List<String>> dumyMap2;
    private ExtHashMap<String, List<String>> dumyResMap;

    @Before
    public void setUp() {
        dumyMap1 = new ExtHashMap<>();
        dumyMap2 = new ExtHashMap<>();
        dumyResMap = new ExtHashMap<>();
        List<String> phoneNums1 = new LinkedList<>(Arrays.asList("123-4567", "456-7890"));
        List<String> phoneNums2 = new LinkedList<>(Arrays.asList("223-4567"));
        List<String> phoneNums3 = new LinkedList<>(Arrays.asList("663-4567"));
        List<String> phoneNums4 = new LinkedList<>(Arrays.asList("123-4567", "456-7890", "123-4567", "456-7890"));
        List<String> phoneNums5 = new LinkedList<>(Arrays.asList("123-4567", "456-7890", "663-4567"));

        dumyMap1.put("001", phoneNums1);
        dumyMap1.put("002", phoneNums2);
        dumyMap2.put("001", phoneNums3);
        dumyMap2.put("101", phoneNums4);

        dumyResMap.put("001", phoneNums5);
        dumyResMap.put("002", new LinkedList<>(phoneNums2));
        dumyResMap.put("101", new LinkedList<>(phoneNums4));

        MergeCSV.clearWorkQueue();
        MergeCSV.insertWorkQueue(dumyMap1);
        MergeCSV.insertWorkQueue(dumyMap2);
    }

    @After
    public void cleanUp() {
        MergeCSV.clearWorkQueue();
    }

    /**
     * This test is to verify the result of merging two HashMap is correct
     */
    @Test
    public void testMergeHashMap() throws InterruptedException {
        Runnable thread = new MergeHashMap();
        ExecutorService executor = Executors.newFixedThreadPool(1);
        executor.execute(thread);
        executor.shutdown();

        // wait current thread to be finished tasks for 2 sec
        if(!executor.awaitTermination(2, TimeUnit.SECONDS)){
            // force them to quit by interrupting
            executor.shutdownNow();
        }
        ExtHashMap<String, List<String>> mergedMap = MergeCSV.pollWorkQueue();
        assertNotNull(mergedMap);

        for(Map.Entry<String, List<String>> cur: mergedMap.entrySet()){
            List<String> expectedList = dumyResMap.get(cur.getKey());
            List<String> returnedList = cur.getValue();
            Collections.sort(expectedList);
            Collections.sort(returnedList);
            assertEquals(true, expectedList.equals(returnedList));
        }
    }



}
