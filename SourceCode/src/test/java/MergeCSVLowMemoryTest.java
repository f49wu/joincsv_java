import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class MergeCSVLowMemoryTest {

    private static String nameFilePath;
    private static String phoneFilePath;
    private static String outputFilePath;
    private static String expectedOutputFilePath;


    @Before
    public void setUp() {
        MergeCSV.clearWorkQueue();

        // get a temporary directory
        String tmpDir = System.getProperty("java.io.tmpdir");
        String uuid = UUID.randomUUID().toString().replace("-", "");
        String curTimeStamp = new SimpleDateFormat("yyyyMMddHHmm'.csv'").format(new Date());
        File tmpOutputFile = new File(tmpDir, "Output_" + uuid + curTimeStamp);
        outputFilePath = tmpOutputFile.getPath();

        File nameFile = new File(getClass().getResource("NameMed.csv").getFile());
        nameFilePath = nameFile.getPath();

        File phoneFile = new File(getClass().getResource("PhoneMed.csv").getFile());
        phoneFilePath = phoneFile.getPath();

        File expectedOutoutFile = new File(getClass().getResource("ExpectedOutput.csv").getFile());
        expectedOutputFilePath = expectedOutoutFile.getPath();
    }

    @After
    public void cleanUp() {
        MergeCSV.clearWorkQueue();
        File tmpOutputFile = new File(outputFilePath);
        tmpOutputFile.delete();
    }

    /**
     * This method triggers MergeCSVLowMemory run over src/test/resources/NameMed.csv
     * and src/test/resources/PhoneMed.csv and compare the result output withe the expected one
     */
    @Test
    public void testMergeCSVLowMemory() throws IOException {
        MergeCSVLowMemory.startMerge(nameFilePath, phoneFilePath, outputFilePath);

        // compare result file with the expected output
        File output = new File(outputFilePath);
        File expectedOutput = new File(expectedOutputFilePath);
        assertEquals(true, FileUtils.contentEquals(output, expectedOutput));
    }

}
