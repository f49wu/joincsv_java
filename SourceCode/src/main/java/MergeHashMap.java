import java.util.List;
import java.util.Map;

/**
 * This class implements functions to merge two hashMap into one, input hashMaps can have duplicate key,
 * this is the consumer for processing outputs from ProcessPhoneRecord objects (producers)
 */
public class MergeHashMap extends Thread {

    /**
     * This method is used to trigger the current thread to run
     * job for merging two hash tables
     */
    @Override
    public void run() {
        // Since we need exactly N-1 merges, then it is guaranteed that this consumer will succeed once
        // so the loop will stop once this consumer done its job
        while (true) {
            // Try polling one subHashMap from the work queue
            ExtHashMap<String, List<String>> subHashMap1 = MergeCSV.pollWorkQueue();
            if (subHashMap1 == null) {
                // retry if we can't poll the first subHashMap
                continue;
            }
            ExtHashMap<String, List<String>> subHashMap2 = MergeCSV.pollWorkQueue();
            if (subHashMap2 == null) {
                // retry if we can't poll the second subHashMap,
                // put back the first subHashMap
                MergeCSV.insertWorkQueue(subHashMap1);
                continue;
            }
            // Starts Merging
            ExtHashMap<String, List<String>> resultMap = merge(subHashMap1, subHashMap2);
            MergeCSV.insertWorkQueue(resultMap); // put back the Merged Table to the Work Queue
            return;
        }
    }

    /**
     * This method merges the given two HashMaps and return the merged HashMaps
     *
     * @param subHashMap1 The first subHashMap
     * @param subHashMap2 The second subHashMap
     */
    private ExtHashMap<String, List<String>> merge(ExtHashMap<String, List<String>> subHashMap1,
                                                   ExtHashMap<String, List<String>> subHashMap2) {
        int subHashMapSize1 = subHashMap1.size();
        int subHashMapSize2 = subHashMap2.size();

        // Select the smaller HashMap to iterate through (nested loop join)
        int minSize = Math.min(subHashMapSize1, subHashMapSize2);
        ExtHashMap<String, List<String>> resultMap;
        if (minSize == subHashMapSize1) {
            resultMap = subHashMap2;
            // update the maxNumPhone by the maxNumPhone values from two subMaps
            resultMap.maxNumPhone = Math.max(subHashMap1.maxNumPhone, subHashMap2.maxNumPhone);
            for (Map.Entry<String, List<String>> curPair : subHashMap1.entrySet()) {
                if (resultMap.containsKey(curPair.getKey())) {
                    // if the nameID already exists, add all new phone records
                    List<String> curList = resultMap.get(curPair.getKey());
                    curList.addAll(curPair.getValue());
                    resultMap.maxNumPhone = Math.max(resultMap.maxNumPhone, curList.size());
                } else {
                    // if the nameID does not exist, just need to insert the old phoneRecord List
                    resultMap.put(curPair.getKey(), curPair.getValue());
                }
            }
        } else {
            resultMap = subHashMap1;
            resultMap.maxNumPhone = Math.max(subHashMap1.maxNumPhone, subHashMap2.maxNumPhone);
            for (Map.Entry<String, List<String>> curPair : subHashMap2.entrySet()) {
                if (resultMap.containsKey(curPair.getKey())) {
                    List<String> curList = resultMap.get(curPair.getKey());
                    curList.addAll(curPair.getValue());
                    resultMap.maxNumPhone = Math.max(resultMap.maxNumPhone, curList.size());
                } else {
                    resultMap.put(curPair.getKey(), curPair.getValue());
                }
            }
        }
        return resultMap;
    }


}
