import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;


/**
 * This class implements functions to produce the Output.csv for the given Name.csv and Phone.csv
 * and this algorithm would prioritize reducing latency over memory consumption
 * Algorithm:
 * 1. Read Phone.csv line by line and pack every chunkSize of rows of PhoneRecord into a List
 * 2. Start a ProcessPhoneRecords(thread) to build HashMap(NameID -> List of PhoneNumbers) for each chunk of PhoneRecord
 * 3. For every two HashMaps that produced by ProcessPhoneRecords(thread), start a MergeHashMap(thread) to merge them
 * 4. After merging, read through Name.csv and one writer would add phone numbers for each NameID
 */
class MergeCSV {

    // WorkQueue for processing subHashMap produced by ProcessPhoneRecord (producer)
    private static ConcurrentLinkedQueue<ExtHashMap<String, List<String>>> phoneRecordsWorkQueue =
            new ConcurrentLinkedQueue<>();

    // Logger for the current class
    private static final Logger logger = LogManager.getLogger(MergeCSV.class);

    // insert a new HashMap into Work Queue
    public static void insertWorkQueue(ExtHashMap<String, List<String>> subHashMap){
        phoneRecordsWorkQueue.add(subHashMap);
    }

    // poll a HashMap from Work Queue
    public static ExtHashMap pollWorkQueue(){
        return phoneRecordsWorkQueue.poll();
    }

    // remove all elements in the Work Queue
    public static void clearWorkQueue(){
        phoneRecordsWorkQueue.clear();
    }

    /**
     * This method produces Output.csv based on the given Name.csv and Phone.csv
     *
     * @param nameFilePath   The file path to Name.csv
     * @param phoneFilePath  The file path to Phone.csv
     * @param outputFilePath The file path to Output.csv
     */
    public static void startMerge(String nameFilePath, String phoneFilePath, String outputFilePath) {
        // initialize threads pool for ProcessRecord (producers) and MergeHasMap (consumers)
        ExecutorService executor = Executors.newFixedThreadPool(Constants.numberThreads);

        BasicConfigurator.configure(); // configure logger

        try {
            logger.info("Start processing Phone.csv...");
            // create csvReader object for Phone.csv
            CSVReader phoneReader = new CSVReader(new FileReader(phoneFilePath));
            String[] curRecord;

            // read data line by line
            int curRow = 0;
            List<PhoneRecord> curRecords = new LinkedList<>();

            // skip the header
            phoneReader.readNext();

            while ((curRecord = phoneReader.readNext()) != null) {
                curRow++;

                // since Phone.csv would not grow in columns
                // and assume Phone.csv is in format "id","phone","name_id"
                // then curRecord is in format "id","phone","name_id"
                curRecords.add(new PhoneRecord(curRecord[2], curRecord[1]));

                // for every chunkSize of rows, start a new thread to process
                if (curRow % Constants.chunkSize == 0) {
                    int curChunkNum = (int) (curRow / Constants.chunkSize);
                    logger.info("Start a new thread for processing Phone.csv: " + curChunkNum);
                    Runnable producer = new ProcessPhoneRecords(curRecords);
                    executor.execute(producer);
                    curRecords = new LinkedList<>();

                    // for N subHashMaps, we need N-1 merges in total
                    // so we skip the first subHashMap and
                    // start merging when 2nd, 3rd...subHashMap is added
                    if (curChunkNum != 1) {
                        logger.info("Start a new thread for merging: " + curChunkNum);
                        Runnable worker = new MergeHashMap();
                        executor.execute(worker);
                    }
                }
            }
            phoneReader.close();

            // the last chunk of PhoneRecords may have less than chunkSize of rows
            if (!curRecords.isEmpty()) {
                int curChunkNum = (int) (curRow / Constants.chunkSize) + 1;
                logger.info("Start a new thread for processing Phone.csv: " + curChunkNum);
                Runnable producer = new ProcessPhoneRecords(curRecords);
                executor.execute(producer);
                if (curChunkNum != 1) {
                    logger.info("Start a new thread for merging: " + curChunkNum);
                    Runnable worker = new MergeHashMap();
                    executor.execute(worker);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        executor.shutdown();

        // waiting for processing and merging to finish
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            logger.error(e.toString());
            return;
        }

        logger.info("Processing Phone.csv is finished, start writing to Output.csv");
        writeToOutput(nameFilePath, outputFilePath);
    }


    /**
     * This method write the result to Output.csv based on the given Name.csv and result HashMap
     *
     * @param nameFilePath   The file path to Name.csv
     * @param outputFilePath The file path to Output.csv
     */
    private static void writeToOutput(String nameFilePath, String outputFilePath) {

        // WorkQueue should only contains one combined HashMap now
        ExtHashMap<String, List<String>> resHashMap = phoneRecordsWorkQueue.poll();
        if (resHashMap == null) {
            logger.error("WorkQueue is empty!");
            return;
        }

        try {
            // create csvWriter for Output.csv
            CSVWriter outputWriter = new CSVWriter(new FileWriter(outputFilePath));

            // create csvReader Name.csv
            CSVReader nameReader = new CSVReader(new FileReader(nameFilePath));
            String[] curRecord = nameReader.readNext(); // read the header of Name.csv

            // get the Maximum number of phones that a NameID can have
            int maxNumPhoneRecord = (int) resHashMap.maxNumPhone;

            // compute the number of headers for Output.csv
            int numHeaders = curRecord.length + maxNumPhoneRecord;

            // compute the header for Output.csv, original headers of Name.csv + phone_1, phone_2...etc.
            String[] newRecord = new String[numHeaders];
            int newRecordIdx = 0;
            for (String header : curRecord) {
                newRecord[newRecordIdx++] = header;
            }
            for (int i = 1; i <= maxNumPhoneRecord; i++) {
                newRecord[newRecordIdx++] = "phone_" + i;
            }
            // write new headers to Output.csv
            outputWriter.writeNext(newRecord);

            while ((curRecord = nameReader.readNext()) != null) {
                // skip the current row, if the current NameID has no phone record
                List<String> phoneNumbers = resHashMap.getOrDefault(curRecord[0], null);
                if (phoneNumbers == null) {
                    continue;
                }

                // copy over the original values in the current row
                int idx = 0;
                for (; idx < curRecord.length; idx++) {
                    newRecord[idx] = curRecord[idx];
                }

                // add phone numbers for the current NameID
                for (int i = 0; i < maxNumPhoneRecord; i++) {
                    if (i >= phoneNumbers.size()) {
                        newRecord[idx++] = "";
                    } else {
                        newRecord[idx++] = phoneNumbers.get(i);
                    }
                }
                outputWriter.writeNext(newRecord);
            }
            outputWriter.close();
            nameReader.close();
        } catch (Exception e) {
            logger.error(e.toString());
            return;
        }

        logger.info("Job succeed, output file path: " + outputFilePath);
    }
}
