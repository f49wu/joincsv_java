import java.util.LinkedList;
import java.util.List;

/**
 * This class implements functions to process the given list of PhoneRecords, and build a HashMap
 * that map each NameID to a list of Phone numbers
 */
public class ProcessPhoneRecords extends Thread {

    private final List<PhoneRecord> phoneRecords;
    private final ExtHashMap<String, List<String>> phoneRecordHashMap;

    /**
     * This is the Constructor for ProcessPhoneRecords class
     *
     * @param curChunk The list of PhoneRecord to process
     */
    public ProcessPhoneRecords(List<PhoneRecord> curChunk) {
        this.phoneRecords = curChunk;
        this.phoneRecordHashMap = new ExtHashMap<>();
    }

    /**
     * This method triggers the thread to run for constructing hashMap
     * based on the given PhoneRecords
     */
    @Override
    public void run() {

        for (PhoneRecord curPair : this.phoneRecords) {
            this.phoneRecordHashMap.putIfAbsent(curPair.nameID, new LinkedList<>());
            List<String> curList = this.phoneRecordHashMap.get(curPair.nameID);
            curList.add(curPair.phoneNum);

            // track the maximum number of phone numbers that a single NameID can have
            if (curList.size() > this.phoneRecordHashMap.maxNumPhone) {
                this.phoneRecordHashMap.maxNumPhone = curList.size();
            }
        }

        // put the HashMap into the work queue so that the consumers(MergeHashMap) can pull
        MergeCSV.insertWorkQueue(this.phoneRecordHashMap);
    }
}

