/**
 * This class represents a single Pair of (nameID, phoneNumber) in the Phone.csv file
 */

public class PhoneRecord {
    public final String nameID;
    public final String phoneNum;

    /**
     * This is the Constructor for PhoneRecord
     *
     * @param nameID   The nameID for the given phone number e.g. 001
     * @param phoneNum This phone number that is associated with the given NameID
     * @return nothing.
     */
    public PhoneRecord(String nameID, String phoneNum) {
        this.nameID = nameID;
        this.phoneNum = phoneNum;
    }
}
