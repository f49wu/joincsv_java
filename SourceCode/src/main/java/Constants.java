public final class Constants {
    public static long chunkSize = 50000000; // number of rows in Phone.csv that a thread process
    public static int numberThreads = 12; // number of threads for producers and consumers
    public static String lowMemoryFlag = "-low-memory"; // flag to indicate run algorithm in low memory mode
    public static String outputFileName = "Output.csv";
}
