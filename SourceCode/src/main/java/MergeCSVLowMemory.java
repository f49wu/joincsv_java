import com.opencsv.*;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * This class implements functions to produce the Output.csv for the given Name.csv and Phone.csv in low memory mode
 * this algorithm would prioritize memory consumption over reducing latency
 * Algorithm:
 * 1. Read chunkSize of rows of Name.csv each time and go through Phone.csv once to build a small HashMap
 * 2. Write Phone Numbers for each NameID in the current chunk, then repeat Step1 until all NameIDs have processed
 * 3. Update the header files of the Output.csv
 */
public class MergeCSVLowMemory {

    // Logger for the current class
    private static final Logger logger = LogManager.getLogger(MergeCSV.class);

    // keep tracking the maximum number of phone that a person can have
    private static int maxNumPhone = 0;

    /**
     * This method produces Output.csv based on the given Name.csv and Phone.csv
     *
     * @param nameFilePath   The file path to Name.csv
     * @param phoneFilePath  The file path to Phone.csv
     * @param outputFilePath The file path to Output.csv
     */
    public static void startMerge(String nameFilePath, String phoneFilePath, String outputFilePath) {
        BasicConfigurator.configure(); // configure logger

        String[] nameCSVHeaders;

        try {
            logger.info("Start processing Name.csv...");

            // create csvReader object for Name.csv
            CSVReader nameReader = new CSVReader(new FileReader(nameFilePath));

            String[] curRecord;

            // read data line by line
            int curRow = 0;

            // the line in Name.csv that last chunk begins with, skip the header
            int lastProcessedLine = 1;

            // record the original headers in Name.csv
            nameCSVHeaders = nameReader.readNext();

            // only keep a HashMap for each chunk of data in Name.csv
            // then clear the current HashMap after write to Outputs.csv
            LinkedHashMap<String, List<String>> curMap = new LinkedHashMap<>();

            // write to a temporary file firstly, then we will update headers later
            String tmpDir = System.getProperty("java.io.tmpdir");
            String curTimeStamp = new SimpleDateFormat("yyyyMMddHHmm'.csv'").format(new Date());
            String uuid = UUID.randomUUID().toString().replace("-", "");
            File tmpOutputFile = new File(tmpDir, "Output_" + uuid + curTimeStamp);

            while ((curRecord = nameReader.readNext()) != null) {
                curRow++;
                // for every chunkSize of rows, iterate through Phone.csv to build a map NameID->PhoneNumbers
                curMap.putIfAbsent(curRecord[0], new LinkedList<>());
                if (curRow % Constants.chunkSize == 0) {
                    int curChunkNum = (int) (curRow / Constants.chunkSize);
                    logger.info("Start processing chunk: " + curChunkNum);
                    processChunk(nameFilePath, phoneFilePath, tmpOutputFile.getPath(), lastProcessedLine, curMap);
                    lastProcessedLine = curRow + 1;
                    curMap.clear(); // clean up the current HashMap

                }
            }

            // process the last chunk
            if (!curMap.isEmpty()) {
                int curChunkNum = (int) (curRow / Constants.chunkSize) + 1;
                logger.info("Start processing chunk: " + curChunkNum);
                processChunk(nameFilePath, phoneFilePath, tmpOutputFile.getPath(), lastProcessedLine, curMap);
            }

            logger.info("Processing Name.csv is finished, start writing to Output.csv");

            // compute new headers for Output.csv
            int newColumnSize = nameCSVHeaders.length + maxNumPhone;
            String[] newRecordBuffer = new String[newColumnSize];
            int newRecordBufferIdx = 0;
            for (String curHeader : nameCSVHeaders) {
                newRecordBuffer[newRecordBufferIdx++] = curHeader;
            }
            for (int i = 1; i <= maxNumPhone; i++) {
                newRecordBuffer[newRecordBufferIdx++] = "phone_" + i;
            }

            CSVReader tmpOutputReader = new CSVReader(new FileReader(tmpOutputFile.getPath()));
            CSVWriter outputWriter = new CSVWriter(new FileWriter(outputFilePath));

            // write new headers back to Output.csv
            outputWriter.writeNext(newRecordBuffer);

            while ((curRecord = tmpOutputReader.readNext()) != null) {
                // copy over each value in temporary output.csv file
                newRecordBufferIdx = 0;
                for (String old : curRecord) {
                    newRecordBuffer[newRecordBufferIdx++] = old;
                }
                // pad remaining column with empty string
                while (newRecordBufferIdx < newColumnSize) {
                    newRecordBuffer[newRecordBufferIdx++] = "";
                }
                outputWriter.writeNext(newRecordBuffer);
            }

            outputWriter.close();
            tmpOutputReader.close();
            nameReader.close();

            logger.info("Job succeed, output file path: " + outputFilePath);

            // remove the temporary output file
            tmpOutputFile.delete();
        } catch (Exception e) {
            logger.error(e.toString());
            return;
        }
    }


    /**
     * This method processes the current chunk of Name.csv by iterating Phone.csv and write result to outputFilePath
     *
     * @param nameFilePath   The file path to Name.csv
     * @param phoneFilePath  The file path to Phone.csv
     * @param outputFilePath The file path to Output.csv (temporary)
     * @param curRow         The row that the current chunk begins with
     * @param curMap         The current HashMap that contains all NameIDs in current chunk
     */
    private static void processChunk(String nameFilePath, String phoneFilePath, String outputFilePath,
                                     int curRow, LinkedHashMap<String, List<String>> curMap) {
        try {

            // create csvReader object for Phone.csv
            CSVReader phoneReader = new CSVReader(new FileReader(phoneFilePath));

            // create csvWriter for Output.csv
            CSVWriter outputWriter = new CSVWriter(new FileWriter(outputFilePath, true)); // append to file

            // create csvReader object for Name.csv, skip first `curRow` rows
            CSVReader nameReader = new CSVReaderBuilder(new FileReader(nameFilePath)).withSkipLines(curRow).build();

            String[] curRecord;
            while ((curRecord = phoneReader.readNext()) != null) {
                // since Phone.csv would not grow in columns
                // and assume Phone.csv is in format "id","phone","name_id"
                // then curRecord is in format "id","phone","name_id"
                if (curMap.containsKey(curRecord[2])) { // skip rows that name_id is not in the curMap
                    List<String> phoneRecords = curMap.get(curRecord[2]);
                    phoneRecords.add(curRecord[1]);
                    maxNumPhone = Math.max(maxNumPhone, phoneRecords.size());
                }
            }

            int curNumRow = 0; // number of rows we've inserted into tmp Output.csv
            while ((curRecord = nameReader.readNext()) != null && curNumRow < Constants.chunkSize) {
                curNumRow++;
                List<String> phoneRecords = curMap.get(curRecord[0]);
                if (phoneRecords.size() == 0) { // skip rows with no phone record
                    continue;
                }
                String[] newRecord = new String[curRecord.length + phoneRecords.size()];

                // copy over the original line in Name.csv and add phone records
                int newRecordIdx = 0;
                for (String oldVal : curRecord) {
                    newRecord[newRecordIdx++] = oldVal;
                }
                for (String phone : phoneRecords) {
                    newRecord[newRecordIdx++] = phone;
                }
                outputWriter.writeNext(newRecord);
            }
            nameReader.close();
            phoneReader.close();
            outputWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
