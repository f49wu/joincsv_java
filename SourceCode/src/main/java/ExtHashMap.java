import java.util.HashMap;

/**
 * This class extends original HashMap and added a new field: maxNumPhone
 * which is the maximum number of phone records that a nameID can have in the map,
 * this field is used to decide the header of output CSV file
 */
public class ExtHashMap<K, V> extends HashMap<K, V> {

    public ExtHashMap() {
        super();
        this.maxNumPhone = 0;
    }

    public long maxNumPhone;
}
