import java.io.File;

/**
 * This class implements functions to parse input strings from the command line interface,
 * then it will trigger either MergeCSV or MergeCSVLowMem based on given parameters
 */
public class MainCLI {

    private static String nameFilePath;
    private static String phoneFilePath;
    private static String outputFilePath;
    private static boolean lowMemoryMode;

    /**
     * This method parse the input arguments and run sanity check
     */
    private static boolean parseInputs(String[] args) {
        int argsLen = args.length;
        // arguments should at least contain <path to Name.csv> and <path to Phone.csv>
        // and there're two optional arguments: <path to output directory>, <low memory mode flag>
        if (argsLen < 2 || argsLen > 4) {
            printHelpInfo();
            return false;
        }

        // first argument is <path to Name.csv>
        File nameFile = new File(args[0]);
        if (!nameFile.exists()) {
            System.out.println(args[0] + " does not exist!");
            return false;
        }
        nameFilePath = nameFile.getPath();

        // second argument is <path to Phone.csv>
        File phoneFile = new File(args[1]);
        if (!phoneFile.exists()) {
            System.out.println(args[1] + " does not exist!");
            return false;
        }
        phoneFilePath = phoneFile.getPath();

        lowMemoryMode = false; // low memory mode is default to off
        boolean outPutDirIsSet = false; // if user has given output directory

        // parse 3rd and 4th arguments in order
        if (argsLen > 2) {
            String thirdArg = args[2];
            if (thirdArg.equals(Constants.lowMemoryFlag)) {
                lowMemoryMode = true;
                if (argsLen > 3) {
                    File outputDir = new File(args[3]);
                    if (!outputDir.exists() || !outputDir.isDirectory()) {
                        System.out.println(args[3] + " is not a valid output directory!");
                        return false;
                    }
                    File outputFile = new File(outputDir, Constants.outputFileName);
                    outputFilePath = outputFile.getPath();
                    outPutDirIsSet = true;
                }
            } else {
                File outputDir = new File(thirdArg);
                if (!outputDir.exists() || !outputDir.isDirectory()) {
                    System.out.println(thirdArg + " is not a valid argument!");
                    return false;
                }
                File outputFile = new File(outputDir, Constants.outputFileName);
                outputFilePath = outputFile.getPath();
                outPutDirIsSet = true;
                if (argsLen > 3) {
                    if (args[3].equals(Constants.lowMemoryFlag)) {
                        lowMemoryMode = true;
                    } else {
                        System.out.println(args[3] + " is not a valid argument!");
                        return false;
                    }
                }
            }
        }

        // if the output directory is not given, set output file path in the current working dir
        if (!outPutDirIsSet) {
            String currentDir = System.getProperty("user.dir");
            File outputFile = new File(currentDir, Constants.outputFileName);
            outputFilePath = outputFile.getPath();
        }

        return true;
    }

    /**
     * This method prints out help info for running CLI
     */
    public static void printHelpInfo() {
        System.out.println(
                "  Usage: java -jar JoinCSV-V1.0.jar <path to Name.csv> <path to Phone.csv> [-optional ...]\n" +
                        "  Optional:\n" +
                        "  <path to output directory>    :: path to the directory where Output.csv will be generated.\n" +
                        "  " + Constants.lowMemoryFlag + "                   :: run join algorithm with low memory consumption."
        );
    }

    public static void main(String[] args) {
        // parse user inputs, exit if failed
        if (!parseInputs(args)) {
            return;
        }

        if (!lowMemoryMode) {
            MergeCSV.startMerge(nameFilePath, phoneFilePath, outputFilePath);
        } else {
            MergeCSVLowMemory.startMerge(nameFilePath, phoneFilePath, outputFilePath);
        }
    }
}
