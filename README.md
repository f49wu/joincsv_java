# JoinCSV

Produces Output.csv from Name.csv and Phone.csv, requirement can be found [here](https://git.uwaterloo.ca/f49wu/joincsv_java/-/blob/master/requirements.pdf).


### Build
This project is built by using Apache Maven. 

Downloading Maven:
- on MacOS with brew
```
brew install maven
```
- on Linux, use `wget` with [urls to zip files](https://maven.apache.org/download.cgi)

- [intallation guide](https://maven.apache.org/install.html)

After Maven is set up
```
1. git clone https://git.uwaterloo.ca/f49wu/joincsv_java.git
2. cd joincsv_java/SourceCode
3. mvn clean compile
4. mvn package
```

### Run
in `SourceCode` directory
```
java -jar ./target/JoinCSV-1.0.jar <path-to-Name.csv> <path-to-Phone.csv>
```
Example:
```
java -jar ./target/JoinCSV-1.0.jar "./src/test/resources/Name.csv" "./src/test/resources/Phone.csv"
```
The above commmand will generate 'Outputs.csv' in the current working directory.

Optional argument `<path-to-ouput-directory>`
```
java -jar ./target/JoinCSV-1.0.jar <path-to-Name.csv> <path-to-Phone.csv> <path-to-output-directory>
```
Example:
```
java -jar ./target/JoinCSV-1.0.jar "./src/test/resources/Name.csv" "./src/test/resources/Phone.csv" "./src/test/resources/"
```
The above commmand will generate 'Output.csv' in the `./src/test/resources/`.

Optional argument `-low-memory`
```
java -jar ./target/JoinCSV-1.0.jar <path-to-Name.csv> <path-to-Phone.csv> -low-memory
```
Example:
```
java -jar ./target/JoinCSV-1.0.jar "./src/test/resources/Name.csv" "./src/test/resources/Phone.csv" -low-memory
```
The above commmand will run algorithm in low memory mode (related to the 2nd question in the requirements) and generate 'Output.csv' in the current working directory.

### Run Unit Tests
in `SourceCode` directory
```
mvn test
```

### Task 1

<em>For this exercise, you are to write the program in Java. The program will read in the input files “name.csv”, “phone.csv” and write out the file “output.csv” as listed above. Beware the input csv files will grow ​in both rows and columns​ in the future as described above. Make sure you consider ​scalability​, code clarity and performance. You can use any Java open source libraries (please list them in a readme file).</em>

### Observations

Input files Name.csv is sorted by ID, Phone.csv is also sorted by its own ID, however, in Phone.csv the `join key` (i.e. `name_id`) is not sorted, thus we can not use merge join algorithm. Consider Name.csv and Phone.csv can both grow by rows, and in this Task, memory is not our top concern, we can choose to use hash join here. Also, in order to compute the correct headers for `Output.csv`, we need to keep tracking the maximum phone number that a single NameID can have. In the final design, we should also consider using parallel computing to make the algorithm more scalable and elastic (i.e. parallel hash join). From the given examples, NameID without phone record should not be in `Output.csv` (i.e. inner join).

Goals for desiging this algorithm:

- #1: Minimize synchronization, avoid taking latches during execution.

- #2: Minimize CPU cache misses, ensure that data is always local to worker thread.

### Algorithm description:

MergeCSV Algorithm (prioritize performance over memory comsumption):

1. Read Phone.csv and pack every chunkSize (we can set chunkSize in `Constants.java`, default to 50M) of rows of PhoneRecord into a List
2. Start a ProcessPhoneRecords(processing thread) to build HashMap(NameID -> List of PhoneNumbers) for each chunk of PhoneRecords
3. For every two HashMaps that produced by ProcessPhoneRecords(thread), start a MergeHashMap(consumer thread) to merge them
4. After Step2 and Step3 are done, read through Name.csv and one writer would write phone numbers for each NameID along with the other values in `Name.csv` into `Output.csv`

Diagram:
<img src="./MergeCSV.png" width="90%">

### Rationale behind parallelizing

Assume a large HashTable contains aggregated Key-Val pairs of 3 subHashTables, and let the size of those 3 subHashTables be `s1`, `s2`, `s3`, without loss of generality, assume `s1 > s2 > s3`. If we have a single thread compute this HashTable the overtime is `W(s1+s2+s3)` (`W(x)` means the time need to process `x` amount of data).

Under the best conditions (sufficient CPU, RAM, multiple cores etc.), we assume two threads are executed rougly simultaneously. Then by parallizing them, at time 0 => 3 threads process `s1`, `s2`, `s3` amount of data simultaneously, takes in total `W(max(s1, s2, s3)) = W(s3)` time, then 2 threads will merge results, say 1 thread merges `s1, s2` which takes `W(s2)` time (we only loop through smaller HashtTables when merging), and after, another thread merge table with `(s1+s2)` data and table with `s3` data which takes `W(min(s1+s2, s3)) = W(s3)` time, so by parallizing, the overall execution time is `W(s2) + W(s3) = W(s2 + s3)` which is less than the original run time `W(s1+s2+s3)`. 

But in reality, we need to consider contextswich overheads, availble CPU, RAM etc. so more splitting may not reduce more latency, thus the intial chunkSize is set as 50M rows (for small amount data, a single thread would be sufficient), and on load testing with 500k rows Name.csv (`/src/test/resources/NameLarge.csv`) and 1M rows of Phone.csv (`/src/test/resources/PhoneLarge.csv`), using 2 threads is faster than using single thread on average.

## Scalability

- Input files Size

As the input file size grow larger (i.e. more rows), we will have more threads (since we split file by `chunkSize` of rows) to process each chunk of data, so the over performance would not be decreased dramatically.

- Vertical Scalable

When we have more computing power, this algorithm would be more effcient as each thread would have more resource to execute.

- Horizontal scalable

If we have more CPU cores, this algorithm would be more effcient as each thread can be executed to run on different cores so that we will achieve nearly true parallelism (i.e. instead of timesharing). Also, we could run logic for processing/merging each chunk of data on different machines (map-reduce).

## Column Size

When writing to `Output.csv`, we need to compute new headers (i.e. NameID, Name, phone_1, phone_2...phone_N). To get how large the column size would be, we need to decide what is the maximum number of phone records that a single NameID cam have. To achieve this, we add one more field `maxNumPhone` in the HashMap class, the we keep tracking the maxNumPhone (`max value size in HashMap`) during the chunk processing and merging. 


## Worst case

The worst case scenario would be a single NameID have a very large amount of phone records, in this case, merging would be very expensive. Empirically, this case is not very reasonable and should be less concerned.

# Taks2
<em> A year later you’ve discovered that your name.csv and phone.csv has grown so large that your program is running out of memory even if you’ve used the largest cloud machine possible. In addition, you’ve been told that from now on you only need to generate a new output.csv once a week (so not a real-time computing requirement). Write the code for how you would adjust your algorithm to handle this new reality. ​Note: since this part is to test your algorithm design, you are therefore not allowed to use a database nor Hadoop to solve this problem. </em>

### Observations
In this task, we need to prioritize memory consumption over reducing latency, so we can process a fixed number of Name IDs in Name.csv each time and go over Phone.csv to find all phone records that associate to those Name IDs. In this way, the memory consumption consumption is bounded.

### Algorithm description (--low-memory):
1. Read chunkSize of rows of Name.csv each time and go through Phone.csv once to build a small HashMap
2. Write Phone Numbers for each NameID in the current chunk, then repeat Step1 until all NameIDs have processed
3. Update the header files of the Output.csv


### Dependencies

-  log4j
-  openCSV
-  Junit
-  Apache.commons


### Further

- consider optimizing number of threads needed, more tests needed